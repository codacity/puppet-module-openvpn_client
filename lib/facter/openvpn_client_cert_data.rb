Facter.add("openvpn_client_cert_data") do
  setcode do
    clients = {}
    path = '/etc/openvpn'
    if File.directory?(path)
      Dir.entries(path).each do |server|
        if File.directory?("#{path}/#{server}/download-configs")
          Dir.entries("#{path}/#{server}/download-configs").each do |client|
            if File.directory?("#{path}/#{server}/download-configs/#{client}") and client !~ /^\.\.?$/ and client !~ /\.tblk$/
              clients["#{server}-#{client}-conf"] = File.open("#{path}/#{server}/download-configs/#{client}/#{client}.conf", "r").read
              clients["#{server}-#{client}-ca"] = File.open("#{path}/#{server}/download-configs/#{client}/keys/#{client}/ca.crt", "r").read
              clients["#{server}-#{client}-crt"] = File.open("#{path}/#{server}/download-configs/#{client}/keys/#{client}/#{client}.crt", "r").read
              clients["#{server}-#{client}-key"] = File.open("#{path}/#{server}/download-configs/#{client}/keys/#{client}/#{client}.key", "r").read
              if File.exists?("#{path}/#{server}/download-configs/#{client}/keys/#{client}/ta.key")
                clients["#{server}-#{client}-ta"] = File.open("#{path}/#{server}/download-configs/#{client}/keys/#{client}/ta.key", "r").read
              end
            end
          end
        end
      end
    end
    clients
  end
end
