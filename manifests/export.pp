define openvpn_client::export (
  $server,
  $tls_auth = false,
) {
  
  Openvpn::Server[$server] ->
  Openvpn::Client[$name] ->
  Openvpn_client::Export[$name]
  
  if $::openvpn_client_cert_data {
    $data = $::openvpn_client_cert_data
  } else {
    fail ('openvpn_client_cert_data not defined, is pluginsync enabled?')
  }
  
  @@file { "${server}-${name}-config":
    path => "/etc/openvpn/${name}.conf",
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '600',
    content => $data["${server}-${name}-conf"],
    tag => "${server}-${name}",
  }
  
  @@file { "${server}-${name}-ca":
    path => "/etc/openvpn/keys/${name}/ca.crt",
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '600',
    content => $data["${server}-${name}-ca"],
    tag => "${server}-${name}",
  }
  
  @@file { "${server}-${name}-crt":
    path => "/etc/openvpn/keys/${name}/${name}.crt",
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '600',
    content => $data["${server}-${name}-crt"],
    tag => "${server}-${name}",
  }

  @@file { "${server}-${name}-key":
    path => "/etc/openvpn/keys/${name}/${name}.key",
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '600',
    content => $data["${server}-${name}-key"],
    tag => "${server}-${name}",
  }

  if $tls_auth {
    @@file { "${server}-${name}-ta":
      path => "/etc/openvpn/keys/${name}/ta.key",
      ensure => file,
      owner => 'root',
      group => 'root',
      mode => '600',
      content => $data["${server}-${name}-ta"],
      tag => "${server}-${name}",
    }
  }
}
