define openvpn_client::import (
  $server,
  $manage_etc = true,
) {
  
  include openvpn_client
  Class['openvpn_client::install'] ->
  Openvpn_client::Import[$name] ~>
  Class['openvpn_client::service']
  
  if ($manage_etc == true) {
    file { ['/etc/openvpn', '/etc/openvpn/keys', "/etc/openvpn/keys/${name}"]:
      ensure  => directory,
      require => Package['openvpn'];
    }
  } else {
    file { "/etc/openvpn/keys/${name}":
      ensure  => directory,
      require => Package['openvpn'];
    }
  }

  File <<| tag == "${server}-${name}" |>>
  
}
